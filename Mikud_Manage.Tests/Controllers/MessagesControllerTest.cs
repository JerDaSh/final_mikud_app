﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mikud_Manage.Controllers;
using Mikud_Manage.Models;
using Mikud_Manage.Models.Repositories;
using Ploeh.AutoFixture;
namespace Mikud_Manage.Tests.Controllers
{
    [TestClass]
    public class MessagesControllerTest
    {
        [TestMethod]
        public void SendMessage()
        {
            IRepository<Message> repository = new RepositoryMessagesMock(null);
            var fix = new Fixture();
            var messagesController = new MessageController(repository);
            var message = new Message
            {
                Sender = "aaa",
                Reciever = "bbb",
                MessageBody = fix.Create<string>()
            };

            messagesController.Create(message);
            Assert.AreEqual(1, repository.GetAll().Count());
        }
    }
}
