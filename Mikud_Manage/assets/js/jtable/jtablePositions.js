﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
$(document).ready(function () {

    var hebrewMessages = {
        serverCommunicationError: 'אירעה שגיאה בעת התקשורת עם השרת.',
        loadingMessage: 'טוען רישום ...',
        noDataAvailable: 'אין רישומים !',
        addNewRecord: 'הוסף רשומה חדשה +',
        editRecord: 'עידכון רשומה',
        areYouSure: 'אתה בטוח?',
        deleteConfirmation: 'רשומה זו תימחק. אתה בטוח?',
        save: 'שמור',
        saving: 'שומר',
        cancel: 'בטל',
        deleteText: 'מחק',
        deleting: 'מוחק',
        error: 'שגיאה',
        close: 'סגור',
        cannotLoadOptionsFor: 'לא ניתן לטעון אפשרויות ל {0{',
        pagingInfo: 'סה"כ: {2}, {0}-{1} מציגים.',
        canNotDeletedRecords: '{0} מן {1} רשומות לא ניתן למחוק!',
        deleteProggress: '{1} מספר {0} נמחק מהרשומות, ממשיך ...'
    };

    $("#PositionTableContainer").jtable({
        title: "טבלת עמדות",
        paging: true,
        messages: hebrewMessages ,
        sorting: true, 
        defaultSorting: "Name ASC",
        actions: {
            listAction: "/PositionsTable/GetAllPositions",
            deleteAction: "/PositionsTable/DeletePosition",
            updateAction: "/PositionsTable/UpdatePosition",
            createAction: "/PositionsTable/CreatePosition"
        },
        fields: {
            ID: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            Name: {
                title: "שם"
            }
        },
        formCreated: function(event, data) {
            data.form.find('input[name="Name"]').addClass(
                'validate[required]');
        },
        formSubmitting: function (event, data) {
            return data.form.validationEngine('validate');
        },
        formClosed: function (event, data) {
            data.form.validationEngine('hide');
            data.form.validationEngine('detach');
        }
    });

    //Load person list from server
    $("#PositionTableContainer").jtable("load");
});