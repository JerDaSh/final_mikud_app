﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
$(document).ready(function () {
    $("#StandardTableContainer").jtable({
        title: "טבלת תקנים",
        paging: true, //Enable paging
        sorting: true, //Enable sorting
        defaultSorting: "Name ASC",
        //openChildAsAccordion: true, //Enable this line to show child tabes as accordion style
        actions: {
            listAction: "/StandardsTable/GetAllStandards",
            deleteAction: "/StandardsTable/DeleteStandard",
            updateAction: "/StandardsTable/UpdateStandard",
            createAction: "/StandardsTable/CreateStandard"
        },
        fields: {
            ID: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            PositionID: {
                title: "שם התקן",
                width: "12%",
                options: "/StandardsTable/GetPositionOptions"
            },
            StartTime: {
                title: "זמן התחלה",
                //type: "date",
                //displayFormat: "HH:mm"
                    display: function (jsonDateString) {
                        var time = new Date(parseInt(jsonDateString.record.StartTime.substr(6),10));
                        return time.getHours()+":"+(time.getMinutes()<10?'0':'') + time.getMinutes();
                }
            },
            EndTime: {
                title: "זמן סיום",
                //type: "date",
                //displayFormat: "HH:mm"
                display: function (jsonDateString) {
                    var time = new Date(parseInt(jsonDateString.record.EndTime.substr(6),10));
                    return time.getHours()+":"+(time.getMinutes()<10?'0':'') + time.getMinutes();
                }
                
            },
            JobID: {
                title: "שם התפקיד",
                width: "12%",
                options: "/StandardsTable/GetJobOptions"
            }
        },
        formCreated: function(event, data) {
            data.form.find('input[name="StartTime"]').addClass(
                'validate[required]');
            data.form.find('input[name="EndTime"]').addClass(
                'validate[required]');

            var $inputStartTime = data.form.find('input[name="StartTime"]');
            $inputStartTime.addClass("time");
            $inputStartTime.timepicker({
                regional: 'he',
                isRTL: true,
                stepMinute: 30
            });

            var $inputEndTime = data.form.find('input[name="EndTime"]');
            $inputEndTime.addClass("time");
            $inputEndTime.timepicker({
                regional: 'he',
                isRTL: true,
                stepMinute: 30
            });
            //data.form.find('input[name="Password"]').addClass(
            //  'validate[required]');
            //data.form.find('input[name="BirthDate"]').addClass(
            //  'validate[required,custom[date]]');
            //data.form.find('input[name="Education"]').addClass(
            //  'validate[required, custom[email]]');
            //data.form.validationEngine();
        },
        formSubmitting: function (event, data) {
            return data.form.validationEngine('validate');
        },
        formClosed: function (event, data) {
            data.form.validationEngine('hide');
            data.form.validationEngine('detach');
        }
    });

    //Load person list from server
    $("#StandardTableContainer").jtable("load");
});