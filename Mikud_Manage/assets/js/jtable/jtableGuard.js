﻿$(document).ready(function () {

    var hebrewMessages = {
        serverCommunicationError: 'אירעה שגיאה בעת התקשורת עם השרת.',
        loadingMessage: 'טוען רישום ...',
        noDataAvailable: 'אין רישומים !',
        addNewRecord: 'הוסף רשומה חדשה +',
        editRecord: 'עידכון רשומה',
        areYouSure: 'אתה בטוח?',
        deleteConfirmation: 'רשומה זו תימחק. אתה בטוח?',
        save: 'שמור',
        saving: 'שומר',
        cancel: 'בטל',
        deleteText: 'מחק',
        deleting: 'מוחק',
        error: 'שגיאה',
        close: 'סגור',
        cannotLoadOptionsFor: 'לא ניתן לטעון אפשרויות ל {0{',
        pagingInfo: 'סה"כ: {2}, {0}-{1} מציגים.',
        canNotDeletedRecords: '{0} מן {1} רשומות לא ניתן למחוק!',
        deleteProggress: '{1} מספר {0} נמחק מהרשומות, ממשיך ...'
    };



    $("#GuardTableContainer").jtable({
        title: "רשימת המאבטחים",
        messages: hebrewMessages,
        paging: true, //Enable paging
        sorting: true, //Enable sorting
        defaultSorting: "Name ASC",
        //openChildAsAccordion: true, //Enable this line to show child tabes as accordion style
        actions: {
            listAction: "/Demo/GetAllGuards",
            deleteAction: "/Demo/DeleteGuard",
            updateAction: "/Demo/UpdateGuard",
            createAction: "/Demo/CreateGuard"
        },
        fields: {
            ID: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            AspNetUserId: {
                title: "שם משתמש",
                options: "/Demo/GetUsers",
                list: false
            },
            FirstName: {
                title: "שם"
            },
            LastName: {
                title: "שם משפחה"
            },
            Birthday: {
                title: "תאריך לידה",
                type: "date",
                displayFormat: "yy-mm-dd"
            },
            StartWorkingDate: {
                title: "תאריך תחילת העבודה",
                type: "date",
                displayFormat: "yy-mm-dd"

            },
            PhoneNumber: {
                title: "מס' טלאפון"
            },
            JobId: {
                title: "שם התפקיד",
                width: "12%",
                options: "/Demo/GetJobOptions",
                list: false
            },
            Addresses: {
                title: 'כתובות',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (guardData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img class="child-opener-image" src="../assets/css/images/Misc/note.png" title="Edit Address" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#GuardTableContainer').jtable('openChildTable',
                            $img.closest('tr'), //Parent row
                            {
                                title: 'כתובות של:' + guardData.record.FirstName + " " + guardData.record.FirstName,
                                actions: {
                                    listAction: '/Demo/GetGuardAddresses?guardId=' + guardData.record.ID,
                                    deleteAction: '/Demo/DeleteAddress',
                                    updateAction: '/Demo/UpdateAddress',
                                    createAction: '/Demo/CreateAddress'
                                },
                                fields: {
                                    ID: {
                                        key: true,
                                        create: false,
                                        edit: false,
                                        list: false
                                    },
                                    FlatNumber: {
                                        title: 'מס דירה'
                                    },
                                    HouseNumber: {
                                        title: 'מס הבית'
                                    },
                                    Street: {
                                        title: 'רחוב'
                                    },
                                    CityID: {
                                        title: 'עיר',
                                        width: '5%',
                                        options: '/Demo/GetCities'
                                    },
                                    Actual: {
                                        title: 'כתובת הנוחכי',
                                        type: 'checkbox',
                                        values: { 'false': 'כן', 'true': 'לא' },
                                        defaultValue: 'false'
                                    },
                                    GuardID: {
                                        defaultValue: guardData.record.ID,
                                        type: 'hidden',
                                        list: false
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            },
            Files: {
                title: 'קבצים',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                listClass: 'child-opener-image-column',
                display: function (guardData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img class="child-opener-image" src="../assets/css/images/Misc/note.png" title="Edit Course" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#GuardTableContainer').jtable('openChildTable',
                            $img.closest('tr'), //Parent row
                            {
                                title: 'קבצים של:' + guardData.record.FirstName + " " + guardData.record.FirstName,
                                actions: {
                                    listAction: '/Demo/GetGuardFiles?guardId=' + guardData.record.ID,
                                    deleteAction: '/Demo/DeleteFile',
                                    updateAction: '/Demo/UpdateFile',
                                    createAction: '/Demo/CreateFile'
                                },
                                fields: {
                                    FileId: {
                                        key: true,
                                        create: false,
                                        edit: false,
                                        list: false
                                    },
                                    FileName: {
                                        title: 'שם הקובץ'
                                    },
                                    ContentType: {
                                        title: 'סוג תוכן'
                                    },
                                    Content: {
                                        title: 'תוכן'
                                    },
                                    FyleType: {
                                        title: 'סוג קובץ'
                                    },
                                    GuardID: {
                                        defaultValue: guardData.record.ID,
                                        type: 'hidden',
                                        list: false
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });
                    //Return image to show on the person row
                    return $img;
                }
            }

        },
        formCreated: function (event, data) {
            data.form.find('input[name="FirstName"]').addClass(
              'validate[required]');
            data.form.find('input[name="LastName"]').addClass(
              'validate[required]');
            data.form.find('input[name="Birthday"]').addClass(
              'validate[required]');
            data.form.find('input[name="StartWorkingDate"]').addClass(
              'validate[required]');
            data.form.find('input[name="PhoneNumber"]').addClass(
              'validate[required]');
        },
        formSubmitting: function (event, data) {
            return data.form.validationEngine('validate');
        },
        formClosed: function (event, data) {
            data.form.validationEngine('hide');
            data.form.validationEngine('detach');
        }
    });
    $("#GuardTableContainer").jtable("load");
});