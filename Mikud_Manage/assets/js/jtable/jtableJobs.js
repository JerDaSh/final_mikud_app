﻿/// <reference path="../typings/jquery/jquery.d.ts"/>
/// <reference path="~/assets/js/bootstrap-switch.js" />
$(document).ready(function () {
    var hebrewMessages = {
        serverCommunicationError: 'אירעה שגיאה בעת התקשורת עם השרת.',
        loadingMessage: 'טוען רישום ...',
        noDataAvailable: 'אין רישומים !',
        addNewRecord: 'הוסף רשומה חדשה +',
        editRecord: 'עידכון רשומה',
        areYouSure: 'אתה בטוח?',
        deleteConfirmation: 'רשומה זו תימחק. אתה בטוח?',
        save: 'שמור',
        saving: 'שומר',
        cancel: 'בטל',
        deleteText: 'מחק',
        deleting: 'מוחק',
        error: 'שגיאה',
        close: 'סגור',
        cannotLoadOptionsFor: 'לא ניתן לטעון אפשרויות ל {0{',
        pagingInfo: 'סה"כ: {2}, {0}-{1} מציגים.',
        canNotDeletedRecords: '{0} מן {1} רשומות לא ניתן למחוק!',
        deleteProggress: '{1} מספר {0} נמחק מהרשומות, ממשיך ...'
    };




    $("#JobsTableContainer").jtable({
        title: "טבלת תפקידים",
        paging: true,
        sorting: true,
        messages: hebrewMessages,
        defaultSorting: "Name ASC",
        actions: {
            listAction: "/JobsTable/GetAllJobs",
            deleteAction: "/JobsTable/DeleteJob",
            updateAction: "/JobsTable/UpdateJob",
            createAction: "/JobsTable/CreateJob"
        },
        fields: {
            ID: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            Name: {
                title: "שם"
            },
            NeedAtrainning: {
                title: "צריך אימון",
                width: "12%",
                type: "checkbox",
                values: { 'false': "כן", 'true': "לא" },
                defaultValue: "true"
            }
        },
        formCreated: function (event, data) {
            data.form.find("input[name=\"Name\"]").addClass(
                "validate[required]");
            //var $needAtrainningSwitch = data.form.find('input[name="NeedAtrainning"]');
            //$needAtrainningSwitch.addClass("switch");
            //$(".switch").bootstrapSwitch({
            //    'onText': "כן",
            //    'offText': "לא"
            //});
        },


        formSubmitting: function (event, data) {
            return data.form.validationEngine('validate');
        },
        formClosed: function (event, data) {
            data.form.validationEngine('hide');
            data.form.validationEngine('detach');
        }
    });

    //Load person list from server
    $("#JobsTableContainer").jtable("load");
});