﻿$(document).ready(function () {
    function showEventPopup(event) {
        //ClearPopupFormValues();
        var modalObj = $('#responsive').modal(); // initialize
        modalObj.modal('show'); // show
        $('#eventTitle').focus();
    }

    $("#calendar").fullCalendar({
        isRTL: true,
        monthNames: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
        monthNamesShort: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
        dayNames: ["יום ראשון", "יום שני", "יום שלישי", "יום רביעי", "יום חמישי", "יום שישי", "יום שבת"],
        dayNamesShort: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת"],
        buttonText: {
            prev: "&nbsp;&#9668;&nbsp;",
            next: "&nbsp;&#9658;&nbsp;",
            prevYear: "&nbsp;&lt;&lt;&nbsp;",
            nextYear: "&nbsp;&gt;&gt;&nbsp;",
            today: "היום",
            month: "חודש",
            week: "שבוע",
            day: "יום"
        },
        eventColor: "#378006",
        eventTextColor: "blue",
        timeFormat: "H:mm{ - H:mm}",
        axisFormat: "HH:mm",
        firstDay: 0,
        header: {
            left: "title",
            right: "prev,next today,agendaDay,agendaWeek,month"
        },
        editable: false,
        droppable: false,
        slotMinutes: 60,
        disableResizing: false,
        //theme: true,
        defaultView: "agendaWeek",
        events: "/FullCalendar/GetEvents/",
        eventClick: function (event, element) {
            //tooltip.hide();
            console.log('event click');
            $('#eventId').val(event.id);
            $('#eventTitle').val(event.title);
            $('#eventStart').val($.fullCalendar.formatDate(event.start, 'dd/MM/yyyy HH:mm'));
            $('#eventEnd').val($.fullCalendar.formatDate(event.end, 'dd/MM/yyyy HH:mm'));
            $('#eventDate').val($.fullCalendar.formatDate(event.start, 'dd/MM/yyyy'));
            $('#allDayEvent').val(event.allDay);
            showEventPopup(event);
        }
    });

    function clearPopupFormValues() {
        $('#eventId').val();
        $('#eventTitle').val();
        $('#eventStart').val();
        $('#eventDate').val();
        $('#eventEnd').val();
    }

    $('#btnPopupCancel').click(function () {
        clearPopupFormValues();
        var modalObj = $('#responsive').modal(); // initialize
        modalObj.modal('hide'); // hide
    });
    $('#btnPopupSave').click(function () {

        var modalObj = $('#responsive').modal(); // initialize
        modalObj.modal('hide'); // hide

        var dataRow = {
            'id':       $('#eventId').val(),
            'title':    $('#eventTitle').val(),
            'start':    $('#eventStart').val(),
            'date':     $('#eventDate').val(),
            'end':      $('#eventEnd').val(),
            'allDay':   $('#allDayEvent').is(':checked')
        }
        clearPopupFormValues();

        $.ajax({
            type: 'POST',
            url: "/Calendar/CreateNewEvent",
            data: dataRow,
            success: function (response) {
                console.log('----------response start-------------');
                console.log(response);
                console.log('----------response end-------------');
                if (response != null) {
                    $('#calendar').fullCalendar('refetchEvents');
                    alert('השינוים נקלתו!');
                } else {
                    alert('שגיע, לא ניתן לשמור אירוע!');
                }
            }
        });
    });
});
