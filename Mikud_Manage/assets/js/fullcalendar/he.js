﻿(function (e) {
    "function" == typeof define && define.amd ? define(["jquery", "moment"], e) : e(jQuery, moment)
})(function (e, t) {
    t.lang("he", {
        months: "ינואר_פברואר_מרץ_אפריל_מאי_יוני_יולי_אוגוסט_ספטמבר_אוקטובר_נובמבר_דצמבר".split("_"),
        monthsShort: "ינו'_פבר'_מרץ_אפר'_מאי_יוני_יולי_אוג'_ספט'_אוק'_נוב'_דצמ'".split("_"),
        weekdays: "ראשון_שני_שלישי_רביעי_חמישי_שישי_שבת".split("_"),
        weekdaysShort: "א_ב_ג_ד_ה_ו_ש".split("_"),
        weekdaysMin: "א_ב_ג_ד_ה_ו_ש".split("_"),
        longDateFormat: {
            LT: "HH:mm",
            L: "DD/MM/YYYY",
            LL: "D MMMM YYYY",
            LLL: "D MMMM YYYY LT",
            LLLL: "dddd, D MMMM YYYY LT"
        },
        calendar: {
            sameDay: "[היום ב] LT",
            nextDay: "[מחר ב] LT",
            nextWeek: "dddd [ב] LT",
            lastDay: "[אתמול ב] LT",
            lastWeek: " dddd [ב] LT",
            sameElse: "L"
        },
        relativeTime: {
            future: "בעוד %s",
            past: "לפני %s",
            s: "לפני שניה",
            m: "דקה",
            mm: "%d דקות",
            h: "שעה",
            hh: "%d שעות",
            d: "יום",
            dd: "%d ימים",
            M: "חודש",
            MM: "%d חודשים",
            y: "שנה",
            yy: "%d שנים"
        },
        week: {
            dow: 1,
            doy: 4
        }
    }), e.fullCalendar.datepickerLang("he", "Hebrew", {
        closeText: "סיום",
        prevText: "הקודם",
        nextText: "הבא",
        currentText: "היום",
        monthNames: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
        monthNamesShort: ["ינו'", "פבר'", "מרץ", "אפר'", "מאי", "יוני", "יולי", "אוג'", "ספט'", "אוק'", "נוב'", "דצמ'"],
        dayNames: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת"],
        dayNamesShort: ["א", "ב", "ג", "ד", "ה", "ו", "ש"],
        dayNamesMin: ["א", "ב", "ג", "ד", "ה", "ו", "ש"],
        weekHeader: "שבוע",
        dateFormat: "dd/mm/yy",
        firstDay: 0,
        isRTL: !0,
        showMonthAfterYear: !1,
        yearSuffix: ""
    }), e.fullCalendar.lang("he", {
        defaultButtonText: {
            month: "חודש",
            week: "שבוע",
            day: "יום",
            list: "רשימה"
        },
        allDayText: "כל היום",
        columnFormat: {
            week: "ddd D/M"
        }
    })
});