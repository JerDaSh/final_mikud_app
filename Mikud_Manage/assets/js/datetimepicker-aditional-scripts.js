﻿$(document).ready(function () {
    $("#datepicker").datepicker({
        closeText: "סיום",
        prevText: "הקודם",
        nextText: "הבא",
        currentText: "היום",
        monthNames: ["ינואר", "פברואר", "מרץ", "אפריל", "מאי", "יוני", "יולי", "אוגוסט", "ספטמבר", "אוקטובר", "נובמבר", "דצמבר"],
        monthNamesShort: ["ינו'", "פבר'", "מרץ", "אפר'", "מאי", "יוני", "יולי", "אוג'", "ספט'", "אוק'", "נוב'", "דצמ'"],
        dayNames: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת"],
        dayNamesShort: ["א", "ב", "ג", "ד", "ה", "ו", "ש"],
        dayNamesMin: ["א", "ב", "ג", "ד", "ה", "ו", "ש"],
        weekHeader: "שבוע",
        dateFormat: "dd/mm/yy",
        firstDay: 0,
        isRTL: true,
        showMonthAfterYear: false,
        yearSuffix: "",
        inline: true,
        onSelect: function (dateText, inst) {
            var d = new Date(dateText);
            $("#calendar").fullCalendar("gotoDate", d);
        }
    });
})