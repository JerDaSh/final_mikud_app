﻿
namespace Mikud_Manage.ViewModels
{
    public class GuardViewModel
    {
        public int ID { get; set; }
        public string AspNetUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }
        public string StartWorkingDate { get; set; }
        public int Points { get; set; }
        public string JobID { get; set; }
        public string Birthday { get; set; }
    }
}