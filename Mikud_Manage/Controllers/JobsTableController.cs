﻿using Mikud_Manage.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    public class JobsTableController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllJobs()
        {
            var jobs = new List<object>();
            foreach (var st in db.Jobs)
            {
                jobs.Add(new
                {
                    ID = st.ID,
                    Name = st.Name,
                    NeedAtrainning = st.NeedAtrainning
                });
            }
            var jobsCount = jobs.Count;
            return Json(new { Result = "OK", Records = jobs, TotalRecordCount = jobsCount }, JsonRequestBehavior.AllowGet);
        }

        //-----------------------Jobs--------------------------------
        public JsonResult GetJob(int jobId)
        {
            var jobs = db.Jobs.ToList<Job>().Where(k => k.ID.Equals(jobId)).Select(e => new
            {
                Name = e.Name,
                ID = e.ID,
                NeedAtrainning = e.NeedAtrainning
            }).ToList();
            var filesCount = jobs.Count;
            return Json(new { Result = "OK", Records = jobs, TotalRecordCount = filesCount });
        }

        public JsonResult UpdateJob([Bind(Include = "ID, Name")]Job job)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }

                var jobToEdit = db.Jobs.Find(job.ID);
                db.Entry(jobToEdit).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CreateJob(Job job)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                db.Jobs.Add(job);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
            return Json(new { Result = "OK", Record = job });
        }

        [HttpPost]
        public JsonResult DeleteJob(int ID)
        {
            try
            {
                var jobToDelete = db.Jobs.Find(ID);
                if (!jobToDelete.Equals(null))
                {
                    db.Jobs.Remove(jobToDelete);
                    db.SaveChanges();
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

