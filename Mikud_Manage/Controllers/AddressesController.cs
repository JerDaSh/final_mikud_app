﻿using Microsoft.AspNet.Identity;
using Mikud_Manage.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    public class AddressesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationDbContext adb = new ApplicationDbContext();


        // GET: Addresses
        public ActionResult Index()
        {

            if (User.IsInRole("Admin") || User.IsInRole("SuperUser"))
            {
                var addresses = db.Addresses.Include(a => a.City).Include(a => a.Guard);
                return View(addresses.ToList());
            }
            else if (User.IsInRole("User"))
            {
                string currentUserId = User.Identity.GetUserId();
                ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
                var scheduals = db.Addresses.Include(a => a.City).Include(a => a.Guard).Where(e => (e.Guard.AspNetUserId.Equals(currentUserId)));
                return View(scheduals.ToList());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: Addresses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // GET: Addresses/Create
        public ActionResult Create()
        {
            ViewBag.CityID = new SelectList(db.Cities, "ID", "Name");
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName");
            return View();
        }

        // POST: Addresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FlatNumber,HouseNumber,Street,CityID,Actual,GuardID")] Address address)
        {
            if (ModelState.IsValid)
            {
                db.Addresses.Add(address);
                db.SaveChanges();
                ViewBag.ResultMessage = "הכתובת נוצרה בהצלחה!";
                Models.Log l = new Models.Log();
                String currentUserId = User.Identity.GetUserId();
                var currentUser = db.Guards.Include(g => g.AspNetUser).Where(g => g.AspNetUserId.Equals(currentUserId)).ToList();
                l.Time = DateTime.Now;
                l.Descreption = "משתמש " + currentUser.FirstOrDefault().FirstName + " " + currentUser.FirstOrDefault().LastName + " יצר כתובת עבור " + address.Guard.FirstName + " " + address.Guard.LastName + "\n";
                l.Descreption += "מספר דירה: " + address.FlatNumber + "\n"
                                + "מספר בית: " + address.HouseNumber + "\n"
                                + "עיר: " + db.Cities.Find(address.CityID).Name + "\n"
                                + "רחוב: " + address.Street + "\n"
                                + "עדקנית: " + address.Actual;

                db.Logs.Add(l);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CityID = new SelectList(db.Cities, "ID", "Name", address.CityID);
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName", address.GuardID);
            return View(address);
        }

        // GET: Addresses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityID = new SelectList(db.Cities, "ID", "Name", address.CityID);
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName", address.GuardID);
            return View(address);
        }

        // POST: Addresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FlatNumber,HouseNumber,Street,CityID,Actual,GuardID")] Address address)
        {
            if (ModelState.IsValid)
            {
                Address addressOld = adb.Addresses.Include(a => a.Guard).Where(a => a.ID.Equals(address.ID)).FirstOrDefault();
                Models.Log l = new Models.Log();
                String currentUserId = User.Identity.GetUserId();
                var currentUser = adb.Guards.Include(g => g.AspNetUser).Where(g => g.AspNetUserId.Equals(currentUserId)).ToList();
                l.Time = DateTime.Now;
                l.Descreption = "משתמש " + currentUser.FirstOrDefault().FirstName + " " + currentUser.FirstOrDefault().LastName + " שינה כתובת עבור " + addressOld.Guard.FirstName + " " + addressOld.Guard.LastName + "\n ישנה ";
                l.Descreption += "מספר דירה: " + addressOld.FlatNumber + "\n"
                                + "מספר בית: " + addressOld.HouseNumber + "\n"
                                + "עיר: " + db.Cities.Find(addressOld.CityID).Name + "\n"
                                + "רחוב: " + addressOld.Street + "\n"
                                + "עדקנית: " + addressOld.Actual + " חדשה \n";
                l.Descreption += "מספר דירה: " + address.FlatNumber + "\n"
                                + "מספר בית: " + address.HouseNumber + "\n"
                                + "עיר: " + db.Cities.Find(address.CityID).Name + "\n"
                                + "רחוב: " + address.Street + "\n"
                                + "עדקנית: " + address.Actual;
                adb.Logs.Add(l);
                adb.SaveChanges();

                db.Entry(address).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityID = new SelectList(db.Cities, "ID", "Name", address.CityID);
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName", address.GuardID);
            return View(address);
        }

        // GET: Addresses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Address address = db.Addresses.Find(id);
            if (address == null)
            {
                return HttpNotFound();
            }
            return View(address);
        }

        // POST: Addresses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Address address = db.Addresses.Include(a => a.Guard).Where(a => a.ID.Equals(id)).FirstOrDefault();
            Models.Log l = new Models.Log();
            String currentUserId = User.Identity.GetUserId();
            var currentUser = db.Guards.Include(g => g.AspNetUser).Where(g => g.AspNetUserId.Equals(currentUserId)).ToList();
            l.Time = DateTime.Now;
            l.Descreption = "משתמש " + currentUser.FirstOrDefault().FirstName + " " + currentUser.FirstOrDefault().LastName + " מחק כתובת עבור " + address.Guard.FirstName + " " + address.Guard.LastName + "\n";
            l.Descreption += "מספר דירה: " + address.FlatNumber + "\n"
                            + "מספר בית: " + address.HouseNumber + "\n"
                            + "עיר: " + db.Cities.Find(address.CityID).Name + "\n"
                            + "רחוב: " + address.Street + "\n"
                            + "עדקנית: " + address.Actual;
            db.Logs.Add(l);
            db.Addresses.Remove(address);
            db.SaveChanges();


            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
