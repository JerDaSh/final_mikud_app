﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Mikud_Manage.Hubs;
using Mikud_Manage.Models;
using Mikud_Manage.Models.Repositories;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    public class InboxController : BaseController
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private readonly IRepository<Message> _iRepository;
        private readonly IRepository<Guard> _iRepositoryGuard;
        public InboxController()
        {
            _iRepositoryGuard = new RepositoryGuards(new ApplicationDbContext());
            _iRepository = new RepositoryMessages(new ApplicationDbContext());
        }
        public InboxController(IRepoContainer repoContainer, IRepository<Guard> iRepositoryGuard)
            : base(repoContainer)
        {
            _iRepositoryGuard = iRepositoryGuard;
        }

        //
        // GET: /Inbox/
        public ActionResult Index()
        {
            var messages = RepoContainer.RepositoryMessage.Get(e => e.Reciever.Equals(User.Identity.GetUserName())).ToList();
            return View(messages);
        }

        public ActionResult Details(int id)
        {
            var message = RepoContainer.RepositoryMessage.Get(id);
            message.IsRead = "true";
            return View(message);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.AspNetUserId = new SelectList(_iRepositoryGuard.GetAll());
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Reciever, Subject, MessageBody")] Message message)
        {
            if (ModelState.IsValid)
            {
                message.Sender = User.Identity.GetUserName(); ;
                message.DateSend = DateTime.Now;
                message.IsRead = "false";
                _iRepository.Add(message);
                IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<MailHub>();
                hubContext.Clients.All.checkMail();
                return RedirectToAction("Index");
            }
            return View(message);
        }

        public ActionResult GetNewMessages(string reciever)
        {
            var messages =
                RepoContainer.RepositoryMessage.Get(e => e.Reciever.Equals(reciever) && e.IsRead.Equals("false"))
                    .OrderByDescending(e => e.DateSend)
                    .FirstOrDefault();
            return PartialView("_GetNewMessages", messages);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int messageId)
        {
            _iRepository.Delete(messageId);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Respond(int messageId)
        {
            var message = _iRepository.Get(messageId);
            var temp = message.Reciever;
            message.Reciever = message.Sender;
            message.Sender = temp;
            return View(message);
        }

    }

}