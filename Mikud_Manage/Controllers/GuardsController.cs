﻿#region

using Microsoft.AspNet.Identity;
using Mikud_Manage.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using File = Mikud_Manage.Models.File;

#endregion

namespace Mikud_Manage.Controllers
{
    [Authorize]
    public class GuardsController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();
        private readonly ApplicationDbContext imdb = new ApplicationDbContext();

        public ActionResult IndexTable()
        {
            return View();
        }

        // GET: Guards
        public ActionResult Index()
        {
            if (User.IsInRole("Admin") || User.IsInRole("SuperUser"))
            {
                var guards = db.Guards.Include(g => g.AspNetUser).Include(g => g.Job).Include(g => g.Files);
                return View(guards.ToList());
            }
            if (User.IsInRole("User"))
            {
                string currentUserId = User.Identity.GetUserId();
                ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
                var guard = db.Guards.FirstOrDefault(e => (e.AspNetUserId.Equals(currentUserId)));
                if (guard != null) return Redirect("Details/" + guard.ID);
                return Redirect("Home");
            }
            return RedirectToAction("Login", "Account");
        }
        [Authorize(Roles = "User")]
        public ActionResult Chat()
        {
            var guards = db.Guards.Include(g => g.AspNetUser).Include(g => g.Job).Include(g => g.Files);
            return View(guards);
        }
        // GET: Guards/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guard guard =
                db.Guards.Include(g => g.AspNetUser)
                    .Include(g => g.Job)
                    .Include(s => s.Files)
                    .SingleOrDefault(s => s.ID == id);
            if (guard == null)
            {
                return HttpNotFound();
            }
            return View(guard);
        }

        // GET: Guards/Create
        public ActionResult Create()
        {
            ViewBag.AspNetUserId = new SelectList(db.Users, "Id", "UserName");
            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name");
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(
            [Bind(Include = "ID,AspNetUserId,FirstName,LastName,Birthday,StartWorkingDate,PhoneNumber,Points,JobID")] Guard guard, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var avatar = new File
                    {
                        FileName = Path.GetFileName(upload.FileName),
                        FileType = FileType.Avatar,
                        ContentType = upload.ContentType
                    };
                    using (var reader = new BinaryReader(upload.InputStream))
                    {
                        avatar.Content = reader.ReadBytes(upload.ContentLength);
                    }
                    guard.Files = new List<File> { avatar };
                }
                db.Guards.Add(guard);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AspNetUserId = new SelectList(db.Users, "Id", "UserName", guard.AspNetUserId);
            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name", guard.JobID);
            return View(guard);
        }

        [Authorize(Roles = "User")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guard guard = db.Guards.Include(s => s.Files).Where(g => g.ID == id).SingleOrDefault(s => s.ID == id);
            if (guard == null)
            {
                return HttpNotFound();
            }
            ViewBag.AspNetUserId = new SelectList(db.Users, "Id", "UserName", guard.AspNetUserId);
            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name", guard.JobID);
            return View(guard);
        }

        [Authorize(Roles = "User")]
        public ActionResult ProfilePage()
        {
            string currentUserId = User.Identity.GetUserId();
            if (currentUserId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
            var guard = db.Guards.FirstOrDefault(e => (e.AspNetUserId.Equals(currentUser.Id)));
            if (guard == null)
            {
                return HttpNotFound();
            }
            ViewBag.AspNetUserId = new SelectList(db.Users, "Id", "UserName", guard.AspNetUserId);
            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name", guard.JobID);
            List<Job> list = db.Jobs.OrderBy(a => a.Name).ToList();
            ViewBag.AllJobs = new SelectList(list, "ID", "Name");
            return View(guard);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public JsonResult ChangeGuardDetails(Guard guard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(guard).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.AspNetUserId = new SelectList(db.Users, "Id", "UserName", guard.AspNetUserId);
            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name", guard.JobID);
            return Json(guard, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        public bool Save(FormCollection formCollection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string currentUserId = User.Identity.GetUserId();
                    var guard = db.Guards.FirstOrDefault(e => (e.AspNetUserId.Equals(currentUserId)));
                    if (Request != null && guard != null)
                    {
                        HttpPostedFileBase file = Request.Files["UploadedFile"];

                        if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                        {
                            var files = imdb.Files.Include(f => f.Guard).Where(f => f.Guard.ID == guard.ID);
                            if (files.Count() != 0)
                            {
                                var firstOrDefault = files.FirstOrDefault();
                                if (firstOrDefault != null)
                                {
                                    var imid = imdb.Files.Find(firstOrDefault.FileId);
                                    imdb.Files.Remove(imid);
                                }
                            }
                            var avatar = new File()
                            {
                                FileName = file.FileName,
                                FileType = FileType.Avatar,
                                ContentType = file.ContentType,
                                GuardID = guard.ID//,
                                //Guard = guard
                            };
                            using (var reader = new BinaryReader(file.InputStream))
                            {
                                avatar.Content = reader.ReadBytes(file.ContentLength);
                            }
                            imdb.Files.Add(avatar);
                            imdb.SaveChanges();
                            imdb.Dispose();
                        }
                        db.Entry(guard).State = EntityState.Modified;
                        db.SaveChanges();

                    }

                }
            }
            catch (Exception)
            {

                return false;
            }

            return true;
        }
        [HttpGet]
        public FileContentResult GetFile()
        {
            try
            {
                string currentUserId = User.Identity.GetUserId();
                var guard = db.Guards.FirstOrDefault(e => (e.AspNetUserId.Equals(currentUserId)));
                File Image = db.Files.Single(e => e.GuardID.Equals(guard.ID));
                return new FileContentResult(Image.Content, Image.ContentType);
            }
            catch (Exception)
            {

                return null;
            }

        }

        [HttpGet]
        public FileContentResult GetFileByUsername(string username)
        {
            try
            {
                var guard = db.Guards.Include(t => t.AspNetUser).FirstOrDefault(e => e.AspNetUser.UserName.Equals(username));
                var image = db.Files.Single(e => e.GuardID.Equals(guard.ID));
                return new FileContentResult(image.Content, image.ContentType);
            }
            catch (Exception)
            {
                return null;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(
            [Bind(Include = "ID,AspNetUserId,FirstName,LastName,Birthday,StartWorkingDate,PhoneNumber,Points,JobID")] Guard guard, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var files = imdb.Files.Include(f => f.Guard).Where(f => f.Guard.ID == guard.ID);
                    if (files.Count() != 0)
                    {
                        var imid = imdb.Files.Find(files.FirstOrDefault().FileId);
                        imdb.Files.Remove(imid);
                    }

                    var avatar = new File
                    {
                        FileName = Path.GetFileName(upload.FileName),
                        FileType = FileType.Avatar,
                        ContentType = upload.ContentType
                    };
                    using (var reader = new BinaryReader(upload.InputStream))
                    {
                        avatar.Content = reader.ReadBytes(upload.ContentLength);
                    }
                    avatar.GuardID = guard.ID;
                    imdb.Files.Add(avatar);
                    imdb.SaveChanges();
                    imdb.Dispose();
                }
                db.Entry(guard).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AspNetUserId = new SelectList(db.Users, "Id", "UserName", guard.AspNetUserId);
            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name", guard.JobID);
            return View(guard);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guard guard = db.Guards.Find(id);
            if (guard == null)
            {
                return HttpNotFound();
            }
            return View(guard);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Guard guard = db.Guards.Include(g => g.AspNetUser).FirstOrDefault(g => g.ID.Equals(id));
            db.Guards.Remove(guard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}