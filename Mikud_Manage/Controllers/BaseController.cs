﻿using System.Data.Entity;
using System.Web.Mvc;
using Mikud_Manage.Models;
using Mikud_Manage.Models.Repositories;

namespace Mikud_Manage.Controllers
{
    public class BaseController : Controller
    {
        public IRepoContainer RepoContainer { get; set; }

        public DbContext MailboxContext { get; set; }

        public BaseController()
        {
            this.MailboxContext = new ApplicationDbContext();
            this.RepoContainer = new RepoContainer(MailboxContext);
        }

        public BaseController(IRepoContainer repoContainer)
        {
            this.MailboxContext = new ApplicationDbContext();
            this.RepoContainer = new RepoContainer(MailboxContext);
        }
    }
}