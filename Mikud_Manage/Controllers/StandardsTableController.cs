﻿using Mikud_Manage.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;


namespace Mikud_Manage.Controllers
{
    public class StandardsTableController : Controller
    {
        // GET: File
        ApplicationDbContext db = new ApplicationDbContext();
        //
        // GET: /File/

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllStandards()
        {
            var standards = new List<object>();
            foreach (var st in db.Standards)
            {
                standards.Add(new
                {
                    ID = st.ID,
                    PositionID = st.PositionID,
                    EndTime = st.EndTime.ToString("HH:mm"),
                    StartTime = st.StartTime.ToString("HH:mm"),
                    JobID = st.JobID
                });
            }
            var standardsCount = standards.Count;
            return Json(new { Result = "OK", Records = standards, TotalRecordCount = standardsCount }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteStandard(int ID)
        {
            try
            {
                var standardToDelete = db.Standards.Find(ID);
                if (!standardToDelete.Equals(null))
                {
                    db.Standards.Remove(standardToDelete);
                    db.SaveChanges();
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public JsonResult UpdateStandard([Bind(Include = "ID, Job, Position, PositionID, EndTime, StartTime, JobID")]Standard standard)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }

                var standardToEdit = db.Standards.Find(standard.ID);
                db.Entry(standardToEdit).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CreateStandard(Standard standard)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                db.Standards.Add(standard);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
            return Json(new { Result = "OK", Record = standard });
        }

        public JsonResult GetJobOptions()
        {
            var jobs = db.Jobs.ToList<Job>().Select(e => new
            {
                Value = e.ID,
                DisplayText = e.Name

            }).ToList();
            var jobsCount = jobs.Count;
            return Json(new { Result = "OK", Options = jobs.OrderBy(e => e.DisplayText) });
        }

        public JsonResult GetPositionOptions()
        {
            var position = db.Positions.ToList<Position>().Select(e => new
            {
                Value = e.ID,
                DisplayText = e.Name

            }).ToList();
            var positionsCount = position.Count;
            return Json(new { Result = "OK", Options = position.OrderBy(e => e.DisplayText) });
        }

        public JsonResult GetUsers()
        {

            var usersDict = db.Users.ToDictionary(e => e.Id, e => e.UserName);
            var users = usersDict.Select(e => new
            {
                Value = e.Key,
                DisplayText = e.Value

            }).ToList();
            var usersCount = users.Count;
            return Json(new { Result = "OK", Options = users.OrderBy(e => e.DisplayText) });
        }

        //-----------------------Positions--------------------------------
        public JsonResult GetStandardPosition(int positionId)
        {
            var positions = db.Positions.ToList<Position>().Where(k => k.ID.Equals(positionId)).Select(e => new
            {
                Scheduals = e.Scheduals,
                Name = e.Name,
                Standards = e.Standards,
                ID = e.ID
            }).ToList();
            var filesCount = positions.Count;
            return Json(new { Result = "OK", Records = positions, TotalRecordCount = filesCount });
        }

        public JsonResult UpdatePosition([Bind(Include = "ID, Name, Sceduals, Standards")]Position position)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }

                var positionToEdit = db.Files.Find(position.ID);
                db.Entry(positionToEdit).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CreatePosition(Position position)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                db.Positions.Add(position);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
            return Json(new { Result = "OK", Record = position });
        }

        [HttpPost]
        public JsonResult DeletePositions(int positionId)
        {
            try
            {
                var positionToDelete = db.Positions.Find(positionId);
                if (!positionToDelete.Equals(null))
                {
                    db.Positions.Remove(positionToDelete);
                    db.SaveChanges();
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }
    }
}
