﻿using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Mikud_Manage.Extensions;
using Mikud_Manage.Models;
using Mikud_Manage.Models.Repositories;

namespace Mikud_Manage.Controllers
{
    [Authorize]
    public class MessageController : BaseController
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        private readonly IRepository<Message> _iRepository;
        private ApplicationDbContext db = new ApplicationDbContext();
        public MessageController()
        {
            _iRepository = new RepositoryMessages(new ApplicationDbContext());
        }

        public MessageController(IRepository<Message> repositoryMessageMock)
        {
            _iRepository = repositoryMessageMock;
        }

        // GET: /Message/
        public ActionResult Index()
        {
            string userIdentityGetUserName = User.Identity.GetUserName();
            var messages = _iRepository.Get(e => e.Reciever.Equals(userIdentityGetUserName)).ToList();
            return View(messages);
        }


        // GET: /Message/Create
        public ActionResult Create()
        {
            ViewBag.AspNetUserId = new SelectList(db.Guards, "Id", "UserName");
            return View();
        }

        // POST: /Message/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Reciever,Subject,MessageBody")] Message message)
        {
            if (ModelState.IsValid)
            {
                message.Sender = User.Identity.GetUserName();
                message.DateSend = DateTimeExtension.DateTimeNow();
                _iRepository.Add(message);
                return RedirectToAction("Index");
            }
            return View(message);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int messageId)
        {
            _iRepository.Delete(messageId);
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Respond(int messageId)
        {
            var message = _iRepository.Get(messageId);
            return View(message);
        }

    }
}
