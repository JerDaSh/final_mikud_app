﻿using Microsoft.AspNet.Identity;
using Mikud_Manage.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    public class LayoutInfoController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();
        // GET: Test
        public ActionResult Index()
        {
            var currentUserId = User.Identity.GetUserId();
            var guard = db.Guards.Include(g => g.AspNetUser).Include(g => g.Job).Include(g => g.Files).FirstOrDefault(e => (e.AspNetUserId.Equals(currentUserId)));
            if (guard != null)
            {
                ViewBag.GuardUsername = guard.AspNetUser.UserName;
                ViewBag.GuardFirstName = guard.FirstName;
                ViewBag.GuardLastName = guard.LastName;
                ViewBag.GuardFileCount = guard.Files.Count;
            }
            return PartialView("_LayoutData");
        }

        public ActionResult MessagesCount()
        {
            var currentUserId = User.Identity.GetUserId();
            var currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
            ViewBag.MsgCnt = currentUser != null ? db.Messages.Count(e => e.Reciever.Equals(currentUser.UserName)) : 0;
            return PartialView("_MessageCount");
        }

        public ActionResult MessagesDropDown()
        {
            var currentUserId = User.Identity.GetUserId();
            var guard = db.Guards.Include(g => g.AspNetUser).Include(g => g.Job).Include(g => g.Files).FirstOrDefault(e => (e.AspNetUserId.Equals(currentUserId)));
            if (guard != null)
            {
                ViewBag.GuardUsername = guard.AspNetUser.UserName;
                ViewBag.GuardFirstName = guard.FirstName;
                ViewBag.GuardLastName = guard.LastName;
                ViewBag.GuardFileCount = guard.Files.Count;
            }
            var currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
            var messages = new List<Message>();
            if ((currentUser != null ? db.Messages.Count(e => e.Reciever.Equals(currentUser.UserName)) : 0) > 0)
            {
                messages = db.Messages.Where(e => e.Reciever.Equals(currentUser.UserName)).ToList();
            }
            ViewBag.MessagesDropDown = messages;
            return PartialView("_MessagesDropDown");
        }
    }
}
