﻿using Mikud_Manage.Models;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    public class FileController : Controller
    {
        // GET: File
        ApplicationDbContext db = new ApplicationDbContext();
        //
        // GET: /File/
        public ActionResult Index(int id)
        {
            var fileToRetrieve = db.Files.Find(id);
            return File(fileToRetrieve.Content, fileToRetrieve.ContentType);
        }

    }
}