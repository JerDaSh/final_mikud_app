﻿using Mikud_Manage.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    public class PositionsTableController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetAllPositions()
        {
            var positions = new List<object>();
            foreach (var st in db.Positions)
            {
                positions.Add(new
                {
                    ID = st.ID,
                    Name = st.Name,
                });
            }
            var positionsCount = positions.Count;
            return Json(new { Result = "OK", Records = positions, TotalRecordCount = positionsCount }, JsonRequestBehavior.AllowGet);
        }

        //-----------------------Positions--------------------------------
        public JsonResult GetPosition(int positionId)
        {
            var positions = db.Positions.ToList<Position>().Where(k => k.ID.Equals(positionId)).Select(e => new
            {
                Name = e.Name,
                ID = e.ID
            }).ToList();
            var filesCount = positions.Count;
            return Json(new { Result = "OK", Records = positions, TotalRecordCount = filesCount });
        }

        public JsonResult UpdatePosition([Bind(Include = "ID, Name")]Position position)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }

                var positionToEdit = db.Positions.Find(position.ID);
                db.Entry(positionToEdit).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CreatePosition(Position position)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                db.Positions.Add(position);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
            return Json(new { Result = "OK", Record = position });
        }

        [HttpPost]
        public JsonResult DeletePositions(int positionId)
        {
            try
            {
                var positionToDelete = db.Positions.Find(positionId);
                if (!positionToDelete.Equals(null))
                {
                    db.Positions.Remove(positionToDelete);
                    db.SaveChanges();
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

