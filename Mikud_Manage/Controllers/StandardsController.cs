﻿using Mikud_Manage.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    [Authorize(Roles = "Admin")]
    public class StandardsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Standards
        public ActionResult Index()
        {
            var standards = db.Standards.Include(s => s.Job).Include(s => s.Position);
            return View(standards.ToList());
        }

        // GET: Standards/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Standard standard = db.Standards.Find(id);
            if (standard == null)
            {
                return HttpNotFound();
            }
            return View(standard);
        }

        // GET: Standards/Create
        public ActionResult Create()
        {
            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name");
            ViewBag.PositionID = new SelectList(db.Positions, "ID", "Name");
            return View();
        }

        // POST: Standards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,StartTime,EndTime,PositionID,JobID")] Standard standard)
        {
            if (ModelState.IsValid)
            {
                db.Standards.Add(standard);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name", standard.JobID);
            ViewBag.PositionID = new SelectList(db.Positions, "ID", "Name", standard.PositionID);
            return View(standard);
        }

        // GET: Standards/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var c = db.Standards.Find(id);
            if (c == null)
            {
                return HttpNotFound();
            }
            var standard = new
            {
                StartTime = c.StartTime.ToShortTimeString(),
                EndTime = c.EndTime.ToShortTimeString(),
                PositionID = c.PositionID,
                JobID = c.JobID,
                ID = c.ID
            };

            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name", c.JobID);
            ViewBag.PositionID = new SelectList(db.Positions, "ID", "Name", c.PositionID);
            return View(standard);
        }

        // POST: Standards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,StartTime,EndTime,PositionID,JobID")] Standard standard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(standard).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.JobID = new SelectList(db.Jobs, "ID", "Name", standard.JobID);
            ViewBag.PositionID = new SelectList(db.Positions, "ID", "Name", standard.PositionID);
            return View(standard);
        }

        // GET: Standards/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Standard standard = db.Standards.Find(id);
            if (standard == null)
            {
                return HttpNotFound();
            }
            return View(standard);
        }

        // POST: Standards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Standard standard = db.Standards.Find(id);
            db.Standards.Remove(standard);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
