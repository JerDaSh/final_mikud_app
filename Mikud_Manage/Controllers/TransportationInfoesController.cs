﻿using Microsoft.AspNet.Identity;
using Mikud_Manage.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    public class TransportationInfoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TransportationInfoes
        public ActionResult Index()
        {

            if (User.IsInRole("Admin") || User.IsInRole("SuperUser"))
            {
                var transportationInfoes = db.TransportationInfoes.Include(t => t.City).Include(t => t.Guard);
                return View(transportationInfoes.ToList());
            }
            else if (User.IsInRole("User"))
            {
                string currentUserId = User.Identity.GetUserId();
                ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
                var scheduals = db.TransportationInfoes.Include(t => t.City).Include(t => t.Guard).Where(e => (e.Guard.AspNetUserId.Equals(currentUserId)));
                return View(scheduals.ToList());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: TransportationInfoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransportationInfo transportationInfo = db.TransportationInfoes.Find(id);
            if (transportationInfo == null)
            {
                return HttpNotFound();
            }
            return View(transportationInfo);
        }

        // GET: TransportationInfoes/Create
        public ActionResult Create()
        {
            ViewBag.CityID = new SelectList(db.Cities, "ID", "Name");
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName");
            return View();
        }

        // POST: TransportationInfoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,HasACar,NeedATransportation,CollectionPoint,CityID,Actual,GuardID")] TransportationInfo transportationInfo)
        {
            if (ModelState.IsValid)
            {
                db.TransportationInfoes.Add(transportationInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CityID = new SelectList(db.Cities, "ID", "Name", transportationInfo.CityID);
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName", transportationInfo.GuardID);
            return View(transportationInfo);
        }

        // GET: TransportationInfoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransportationInfo transportationInfo = db.TransportationInfoes.Find(id);
            if (transportationInfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityID = new SelectList(db.Cities, "ID", "Name", transportationInfo.CityID);
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName", transportationInfo.GuardID);
            return View(transportationInfo);
        }

        // POST: TransportationInfoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,HasACar,NeedATransportation,CollectionPoint,CityID,Actual,GuardID")] TransportationInfo transportationInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transportationInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityID = new SelectList(db.Cities, "ID", "Name", transportationInfo.CityID);
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName", transportationInfo.GuardID);
            return View(transportationInfo);
        }

        // GET: TransportationInfoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TransportationInfo transportationInfo = db.TransportationInfoes.Find(id);
            if (transportationInfo == null)
            {
                return HttpNotFound();
            }
            return View(transportationInfo);
        }

        // POST: TransportationInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TransportationInfo transportationInfo = db.TransportationInfoes.Find(id);
            db.TransportationInfoes.Remove(transportationInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
