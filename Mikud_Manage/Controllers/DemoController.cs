﻿using Mikud_Manage.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;


namespace Mikud_Manage.Controllers
{
    public class DemoController : Controller
    {



        // GET: File
        ApplicationDbContext db = new ApplicationDbContext();
        //
        // GET: /File/
        public JsonResult GetAllGuards()
        {
            var guards = db.Guards.ToList<Guard>().Select(e => new
            {
                ID = e.ID,
                AspNetUserId = e.AspNetUserId,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Birthday = e.Birthday,
                StartWorkingDate = e.StartWorkingDate,
                PhoneNumber = e.PhoneNumber,
                JobID = e.JobID
            }).ToList();
            var guardsCount = guards.Count;
            return Json(new { Result = "OK", Records = guards, TotalRecordCount = guardsCount });
        }

        [HttpPost]
        public JsonResult DeleteGuard(int ID)
        {
            try
            {
                var guardToDelete = db.Guards.Find(ID);
                if (!guardToDelete.Equals(null))
                {
                    db.Guards.Remove(guardToDelete);
                    db.SaveChanges();
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public JsonResult UpdateGuard([Bind(Include = "ID, AspNetUserId, FirstName, LastName, Birthday, StartWorkingDate, PhoneNumber, JobID")]Guard guard)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }

                var guardToEdit = db.Guards.Find(guard.ID);
                db.Entry(guardToEdit).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        // POST: Guards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult CreateGuard(Guard guard)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                db.Guards.Add(guard);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
            return Json(new { Result = "OK", Record = guard });
        }
        public JsonResult GetJobOptions()
        {
            var jobs = db.Jobs.ToList<Job>().Select(e => new
            {
                Value = e.ID,
                DisplayText = e.Name

            }).ToList();
            var jobsCount = jobs.Count;
            return Json(new { Result = "OK", Options = jobs.OrderBy(e => e.DisplayText) });
        }

        public JsonResult GetUsers()
        {

            var usersDict = db.Users.ToDictionary(e => e.Id, e => e.UserName);
            var users = usersDict.Select(e => new
            {
                Value = e.Key,
                DisplayText = e.Value

            }).ToList();
            var usersCount = users.Count;
            return Json(new { Result = "OK", Options = users.OrderBy(e => e.DisplayText) });
        }

        //-----------------------Addresses--------------------------------
        public JsonResult GetGuardAddresses(int guardId)
        {
            var addresses = db.Addresses.ToList<Address>().Where(k => k.GuardID.Equals(guardId)).Select(e => new
            {
                ID = e.ID,
                FlatNumber = e.FlatNumber,
                HouseNumber = e.HouseNumber,
                Street = e.Street,
                CityID = e.CityID,
                Actual = e.Actual,
                GuardID = e.GuardID
            }).ToList();
            var addressesCount = addresses.Count;
            return Json(new { Result = "OK", Records = addresses, TotalRecordCount = addressesCount });
        }

        public JsonResult UpdateAddress([Bind(Include = "ID, FlatNumber, HouseNumber, Street, CityID, Actual, GuardID")]Address address)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }

                var addressToEdit = db.Addresses.Find(address.ID);
                db.Entry(addressToEdit).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CreateAddress(Address address)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                db.Addresses.Add(address);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
            return Json(new { Result = "OK", Record = address });
        }
        public JsonResult GetCities()
        {
            var cities = db.Cities.ToList<City>().Select(e => new
            {
                Value = e.ID,
                DisplayText = e.Name

            }).ToList();
            return Json(new { Result = "OK", Options = cities.OrderBy(e => e.DisplayText) });
        }

        [HttpPost]
        public JsonResult DeleteAddress(int Id)
        {
            try
            {
                var addressToDelete = db.Addresses.Find(Id);
                if (!addressToDelete.Equals(null))
                {
                    db.Addresses.Remove(addressToDelete);
                    db.SaveChanges();
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        //-----------------------Files--------------------------------
        public JsonResult GetGuardFiles(int guardId)
        {
            var files = db.Files.ToList<File>().Where(k => k.GuardID.Equals(guardId)).Select(e => new
            {
                FileId = e.FileId,
                FileName = e.FileName,
                ContentType = e.ContentType,
                GuardID = e.GuardID
            }).ToList();
            var filesCount = files.Count;
            return Json(new { Result = "OK", Records = files, TotalRecordCount = filesCount });
        }

        public JsonResult UpdateFile([Bind(Include = "ID, FileName, ContentType, Content, GuardID")]File file)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }

                var fileToEdit = db.Files.Find(file.FileId);
                db.Entry(fileToEdit).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult CreateFile(File file)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return Json(new { Result = "ERROR", Message = "Form is not valid! Please correct it and try again." });
                }
                db.Files.Add(file);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
            return Json(new { Result = "OK", Record = file });
        }

        [HttpPost]
        public JsonResult DeleteFiles(int FileId)
        {
            try
            {
                var fileToDelete = db.Files.Find(FileId);
                if (!fileToDelete.Equals(null))
                {
                    db.Files.Remove(fileToDelete);
                    db.SaveChanges();
                }
                return Json(new { Result = "OK" });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }







        public ViewResult MasterChild()
        {
            return View();
        }

        public ActionResult UsingWithValidationEngine1()
        {
            return View();
        }

        public ActionResult UsingWithValidationEngine2()
        {
            return View();
        }

        public ActionResult Theme()
        {
            return View();
        }

        public ActionResult TurkishLocalization()
        {
            return View();
        }

        public ActionResult Filtering()
        {
            //var cities = _repository.CityRepository.GetAllCities().Select(city => new SelectListItem {Text = city.CityName, Value = city.CityId.ToString()}).ToList();
            //cities.Insert(0, new SelectListItem {Selected = true, Text = "All cities", Value = "0"});

            //ViewBag.Cities = cities;
            return View();
        }

        public ActionResult ColumnResizing()
        {
            return View();
        }

        public ActionResult ColumnHideShow()
        {
            return View();
        }
    }
}
