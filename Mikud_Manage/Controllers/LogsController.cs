﻿using Mikud_Manage.Models;
using System.Linq;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    [Authorize(Roles = "Admin")]
    public class LogsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Logs
        public ActionResult Index()
        {
            var logs = db.Logs.OrderByDescending(l => l.Time);
            return View(logs.ToList());
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
