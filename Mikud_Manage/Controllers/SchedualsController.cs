﻿using Microsoft.AspNet.Identity;
using Mikud_Manage.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    [Authorize]
    public class SchedualsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Scheduals
        public ActionResult Index()
        {

            if (User.IsInRole("Admin") || User.IsInRole("SuperUser"))
            {
                var scheduals = db.Scheduals.Include(s => s.Guard).Include(s => s.Position).OrderByDescending(s => s.End);
                return View(scheduals.ToList());
            }
            else if (User.IsInRole("User"))
            {
                string currentUserId = User.Identity.GetUserId();
                ApplicationUser currentUser = db.Users.FirstOrDefault(x => x.Id == currentUserId);
                var scheduals = db.Scheduals.Include(s => s.Guard).Include(s => s.Position).Where(e => (e.Guard.AspNetUserId.Equals(currentUserId)));
                return View(scheduals.ToList());
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // GET: Scheduals/Calendar
        /* public ActionResult Calendar()
         {
             var scheduler = new DHXScheduler(this);
             scheduler.Skin = DHXScheduler.Skins.Flat;

             scheduler.Config.first_hour = 6;
             scheduler.Config.last_hour = 20;

             scheduler.LoadData = true;
             scheduler.EnableDataprocessor = true;

             return View(scheduler);
         }

         public ContentResult Data()
         {
             var apps = calDb.Schedual.ToList();
             return new SchedulerAjaxData(apps);
         }

        public ActionResult Save(int? id, FormCollection actionValues)
        {
            var action = new DataAction(actionValues);

            try
            {
                var changedEvent = DHXEventsHelper.Bind<Schedual>(actionValues);
                switch (action.Type)
                {
                    case DataActionTypes.Insert:
                        calDb.Schedual.Add(changedEvent);
                        break;
                    case DataActionTypes.Delete:
                        calDb.Entry(changedEvent).State = EntityState.Deleted;
                        break;
                    default:// "update"  
                        calDb.Entry(changedEvent).State = EntityState.Modified;
                        break;
                }
                calDb.SaveChanges();
                action.TargetId = changedEvent.ID;
            }
            catch (Exception a)
            {
                action.Type = DataActionTypes.Error;
            }

            return (new AjaxSaveResponse(action));
        }*/

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedual schedual = db.Scheduals.Find(id);
            if (schedual == null)
            {
                return HttpNotFound();
            }
            return View(schedual);
        }

        // GET: Scheduals/Create

        [Authorize(Roles = "Admin, SuperUser")]
        public ActionResult Create()
        {
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName");
            ViewBag.PositionID = new SelectList(db.Positions, "ID", "Name");
            return View();
        }

        // POST: Scheduals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Start,End,PositionID,GuardID")] Schedual schedual)
        {
            if (ModelState.IsValid)
            {
                db.Scheduals.Add(schedual);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName", schedual.GuardID);
            ViewBag.PositionID = new SelectList(db.Positions, "ID", "Name", schedual.PositionID);
            return View(schedual);
        }

        // GET: Scheduals/Edit/5
        [Authorize(Roles = "Admin, SuperUser")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedual schedual = db.Scheduals.Find(id);
            if (schedual == null)
            {
                return HttpNotFound();
            }
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName", schedual.GuardID);
            ViewBag.PositionID = new SelectList(db.Positions, "ID", "Name", schedual.PositionID);
            return View(schedual);
        }

        // POST: Scheduals/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Start,End,PositionID,GuardID")] Schedual schedual)
        {
            if (ModelState.IsValid)
            {
                db.Entry(schedual).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GuardID = new SelectList(db.Guards, "ID", "FirstName", schedual.GuardID);
            ViewBag.PositionID = new SelectList(db.Positions, "ID", "Name", schedual.PositionID);
            return View(schedual);
        }

        // GET: Scheduals/Delete/5

        [Authorize(Roles = "Admin, SuperUser")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Schedual schedual = db.Scheduals.Find(id);
            if (schedual == null)
            {
                return HttpNotFound();
            }
            return View(schedual);
        }

        // POST: Scheduals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Schedual schedual = db.Scheduals.Find(id);
            db.Scheduals.Remove(schedual);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                //calDb.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
