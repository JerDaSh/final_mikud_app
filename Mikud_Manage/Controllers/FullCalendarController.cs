﻿using Mikud_Manage.Models;
using Mikud_Manage.Models.Repositories;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    public class FullCalendarController : BaseController
    {
        private readonly IRepository<Schedual> _iRepository;


        public FullCalendarController()
        {
            _iRepository = new RepositoryScheduals(new ApplicationDbContext());
        }
        public FullCalendarController(IRepository<Schedual> iRepository)
        {
            _iRepository = iRepository;
        }


        // GET: FullCalendar
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetEvents(double start, double end)
        {

            var fromDate = ConvertFromUnixTimestamp(start);
            var toDate = ConvertFromUnixTimestamp(end);

            var events = _iRepository.GetAll().ToList();
            var eventList = from e in events
                            select new
                            {
                                id = e.GuardID.ToString(),
                                title = e.Position.Name.ToString(),
                                start = e.Start.ToString("s"),
                                end = e.End.ToString("s"),
                                allDay = false
                            };

            var rows = eventList.ToArray();
            return Json(rows, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllEventsForDates(double start, double end, string ID)
        {
            var fromDate = ConvertFromUnixTimestamp(start);
            var toDate = ConvertFromUnixTimestamp(end);
            var events = _iRepository.GetAll().Where(e => !e.GuardID.Equals(ID)).ToList();
            var eventList = from e in events
                            select new
                            {
                                id = e.GuardID.ToString(),
                                title = e.Position.Name.ToString(),
                                start = e.Start.ToString("s"),
                                end = e.End.ToString("s"),
                                allDay = false
                            };
            var rows = eventList.ToArray();
            return Json(rows, JsonRequestBehavior.AllowGet);
        }
        private static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }
    }
}
