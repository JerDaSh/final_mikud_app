﻿using Less.Core.Model;
using Microsoft.AspNet.Identity;
using Mikud_Manage.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace Mikud_Manage.Controllers
{
    [Authorize(Roles = "User")]
    public class CalendarController : Controller
    {
        private readonly ApplicationDbContext _db = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAll()
        {
            var currentUserId = User.Identity.GetUserId();
            var currentUser = _db.Users.FirstOrDefault(x => x.Id == currentUserId);
            var options = _db.Options.Include(s => s.Guard).Where(e => (e.Guard.AspNetUserId.Equals(currentUserId)));
            var calendarOptions = new List<OptionsCalendarViewModel>();

            foreach (var option in options)
            {
                calendarOptions.Add(new OptionsCalendarViewModel
                {
                    eventId = option.ID.ToString(),
                    guardId = option.GuardID,
                    date = option.Start.ToShortDateString(),
                    title = option.Title,
                    start = option.Start.ToString(CultureInfo.InvariantCulture),
                    end = option.End.ToString(CultureInfo.InvariantCulture),
                    allDay = option.End.Subtract(option.Start).Hours > 23 ? true : false
                });


            };

            return Json(calendarOptions, JsonRequestBehavior.AllowGet);
        }

        public bool AddNew(OptionsCalendarViewModel optionViewModel)
        {
            if (ModelState.IsValid)
            {
                var option = new Option
                {
                    Title = optionViewModel.title,
                    EvenetId = optionViewModel.eventId,
                    GuardID = optionViewModel.guardId,
                    Start = optionViewModel.start.Equals(string.Empty) ? new DateTime() : Convert.ToDateTime(optionViewModel.start),
                    End = optionViewModel.allDay ? optionViewModel.start.Equals(string.Empty) ? new DateTime() : Convert.ToDateTime(optionViewModel.start).AddDays(1) : optionViewModel.end.Equals(string.Empty) ? new DateTime() : Convert.ToDateTime(optionViewModel.end),
                    Guard = _db.Guards.Single(x => x.ID.Equals(optionViewModel.guardId))
                };
                var currentUserId = User.Identity.GetUserId();
                var currentUser = _db.Users.FirstOrDefault(x => x.Id == currentUserId);
                var options = _db.Options.Include(s => s.Guard).Where(e => (e.Guard.AspNetUserId.Equals(currentUserId)));
                if (options.Contains(option)) return false;
                _db.Options.Add(option);
                _db.SaveChanges();
                return true;
            }
            return false;
        }
        public JsonResult CreateNewEvent(OptionsCalendarViewModel optionVm)
        {
            try
            {
                var currentUserId = User.Identity.GetUserId();
                var currentUser = _db.Users.FirstOrDefault(x => x.Id == currentUserId);
                var guard = _db.Guards.FirstOrDefault(e => (e.AspNetUserId.Equals(currentUserId)));
                var convertedDate = DateTime.ParseExact(optionVm.date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var options = _db.Options.Include(s => s.Guard).Where(e => (e.Guard.AspNetUserId.Equals(currentUserId))).ToList();
                var specificDateOptions = options.Where(e => e.Start.Date.Equals(convertedDate)).ToList();
                var timeToAssign = GetOptionTimes(specificDateOptions, optionVm);

                if (guard != null)
                {
                    var optionToAdd = new Option
                    {
                        Guard = guard,
                        GuardID = guard.ID,
                        Start = optionVm.allDay
                            ? DateTime.ParseExact(optionVm.date + " " + "00:00", "dd/MM/yyyy HH:mm",
                                CultureInfo.InvariantCulture)
                            : DateTime.ParseExact(timeToAssign.Split('-')[0], "dd/MM/yyyy HH:mm",
                                CultureInfo.InvariantCulture),
                        End = optionVm.allDay
                            ? DateTime.ParseExact(optionVm.date + " " + "23:59", "dd/MM/yyyy HH:mm",
                                CultureInfo.InvariantCulture)
                            : DateTime.ParseExact(timeToAssign.Split('-')[1], "dd/MM/yyyy HH:mm",
                                CultureInfo.InvariantCulture),
                        Title = optionVm.title
                    };
                    _db.Options.RemoveRange(FindAllOptionsInTimeRange(specificDateOptions, timeToAssign));
                    _db.Options.Add(optionToAdd);
                    _db.SaveChanges();
                }
            }
            catch (Exception)
            {
                return null;
            }
            return Json(optionVm, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteEvent(OptionsCalendarViewModel optionVm)
        {
            try
            {
                var c = Int32.Parse(optionVm.eventId);
                _db.Options.Remove(_db.Options.Single(e => e.ID.Equals(c)));
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return null;
            }
            return Json(optionVm, JsonRequestBehavior.AllowGet);
        }
        private IEnumerable<Option> FindAllOptionsInTimeRange(List<Option> options, string range, Option current = null)
        {
            var startHour = DateTime.ParseExact(range.Split('-')[0], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
            var endHour = DateTime.ParseExact(range.Split('-')[1], "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
            return options.Where(e => (e.Start >= startHour && e.Start <= endHour) || (e.End >= startHour && e.End <= endHour)).Where(e => !e.Equals(current));
        }
        private string GetOptionTimes(List<Option> listOfOptions, OptionsCalendarViewModel optionVm)
        {
            var newStartDateTime = DateTime.ParseExact(optionVm.date + " " + optionVm.start, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
            var newEndDateTime = DateTime.ParseExact(optionVm.date + " " + optionVm.end, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
            var maxDateTime = listOfOptions.Any() ? listOfOptions.Max(e => e.End) : newEndDateTime;
            var minDateTime = listOfOptions.Any() ? listOfOptions.Min(e => e.Start) : newStartDateTime;
            if (newStartDateTime <= minDateTime && newEndDateTime >= maxDateTime ||
                newStartDateTime >= maxDateTime && newEndDateTime >= maxDateTime ||
                newStartDateTime <= minDateTime && newEndDateTime <= minDateTime)
            {
                return newStartDateTime.ToString("dd/MM/yyyy HH:mm") + "-" + newEndDateTime.ToString("dd/MM/yyyy HH:mm");
            }
            if (newStartDateTime <= minDateTime && newEndDateTime >= minDateTime)
            {
                return newStartDateTime.ToString("dd/MM/yyyy HH:mm") + "-" + maxDateTime.ToString("dd/MM/yyyy HH:mm");
            }
            if (newStartDateTime <= maxDateTime && newEndDateTime >= maxDateTime)
            {
                return minDateTime.ToString("dd/MM/yyyy HH:mm") + "-" + newEndDateTime.ToString("dd/MM/yyyy HH:mm");
            }
            return minDateTime.ToString("dd/MM/yyyy HH:mm") + "-" + maxDateTime.ToString("dd/MM/yyyy HH:mm");
        }
        public JsonResult UpdateEvent(OptionsCalendarViewModel optionVm)
        {
            try
            {
                var currentUserId = User.Identity.GetUserId();
                var currentUser = _db.Users.FirstOrDefault(x => x.Id == currentUserId);
                var guard = _db.Guards.FirstOrDefault(e => (e.AspNetUserId.Equals(currentUserId)));
                var convertedDate = DateTime.ParseExact(optionVm.date, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture);
                var eventDate = DateTime.ParseExact(convertedDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var m = convertedDate.Month;
                var d = convertedDate.Day;
                var y = convertedDate.Year;
                var options = _db.Options.Include(s => s.Guard).Where(e => (e.Guard.AspNetUserId.Equals(currentUserId))).ToList();
                var specificDateOptions = options.Where(e => e.Start.Date.Equals(eventDate)).ToList();
                optionVm.date = convertedDate.ToString("dd/MM/yyyy");
                var startTime = DateTime.ParseExact(optionVm.start, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture);
                optionVm.start = startTime.ToString("HH:mm");
                var endTime = DateTime.ParseExact(optionVm.end, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture);
                optionVm.end = endTime.ToString("HH:mm");
                var timeToAssign = GetOptionTimes(specificDateOptions, optionVm);
                if (guard != null && guard.ID == optionVm.guardId)
                {
                    var c = Convert.ToInt32(optionVm.eventId);
                    var optionToAdd = _db.Options.FirstOrDefault(e => e.ID.Equals(c));
                    if (optionToAdd != null)
                    {
                        optionToAdd.Start = optionVm.allDay ? DateTime.ParseExact(optionVm.date + " " + "00:00", "dd/MM/yyyy HH:mm",
                                                CultureInfo.InvariantCulture) : DateTime.ParseExact(timeToAssign.Split('-')[0], "dd/MM/yyyy HH:mm",
                                                CultureInfo.InvariantCulture);
                        optionToAdd.End = optionVm.allDay ? DateTime.ParseExact(optionVm.date + " " + "23:59", "dd/MM/yyyy HH:mm",
                            CultureInfo.InvariantCulture) : DateTime.ParseExact(timeToAssign.Split('-')[1], "dd/MM/yyyy HH:mm",
                            CultureInfo.InvariantCulture);
                        optionToAdd.Title = optionVm.title;

                        _db.Options.RemoveRange(FindAllOptionsInTimeRange(specificDateOptions, timeToAssign, optionToAdd));
                        _db.SaveChanges();
                    }

                }
            }
            catch (Exception)
            {
                return null;
            }
            return Json(optionVm, JsonRequestBehavior.AllowGet);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}