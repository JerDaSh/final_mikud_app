﻿using Microsoft.AspNet.SignalR;

namespace Mikud_Manage.Hubs
{
    public class MailHub : Hub
    {
        public void Send()
        {
            Clients.All.checkMail();
        }
    }
}