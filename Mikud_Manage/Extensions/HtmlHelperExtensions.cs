﻿using System;
using System.Web.Mvc;

namespace Mikud_Manage.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString AuthWarning(this HtmlHelper html, string userName)
        {
            if (String.IsNullOrEmpty(userName))
            {
                return new MvcHtmlString("Please sign in to view your mailbox");
            }
            return new MvcHtmlString("<a href='/Message/Index'>" + "תיבת מייל של:" + userName + "</a>");
        }


    }
}