﻿using System;

namespace Mikud_Manage.Extensions
{
    public static class DateTimeExtension
    {
        public static string DateTimeFormatted(this DateTime dateTime)
        {
            var dtFormated = dateTime.ToString("U");
            return dtFormated;
        }

        public static DateTime DateTimeNow()
        {
            return DateTime.Now;
        }

    }
}