﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Mikud_Manage.Models;
using Mikud_Manage.ViewModels;

namespace Mikud_Manage.Extensions
{
    public static class MessageExtentions
    {
        public static string DateTimeFormatted(this Message message)
        {
            string dtFormatted = message.DateSend.ToString("D");
            return dtFormatted;
        }

        //public static IHtmlString ActiveMessageUser(this HtmlHelper htmlHelper, UserName userName)
        //{
            
        //}
        //public static IHtmlString MessageDetailsLink(this HtmlHelper htmlHelper,)

        public static IHtmlString MessageDetailsLink(this HtmlHelper htmlHelper, Message message)
        {
            string LinkText = message.Sender + ".................." + message.DateSend;
            if (message.IsRead.Equals("false"))
            {
                return htmlHelper.ActionLink(LinkText, "Details", "Inbox", new {id = message.Id}, null);
            }
            return htmlHelper.ActionLink(LinkText, "Details", "Inbox", new { id = message.Id }, null);

        }
    }
}