﻿using Microsoft.AspNet.Identity;
using Mikud_Manage.Models;
using System.Web.Mvc;

namespace Mikud_Manage.Views
{
    public abstract class BaseView : WebViewPage
    {
        public string UserName
        {
            get
            {
                return User != null ? User.Identity.GetUserName() : null;
            }
        }

        public ApplicationDbContext MailboxContext { get; set; }

        protected BaseView()
        {

        }
    }
    
    public abstract class BaseView<TModel> : WebViewPage<TModel> where TModel : class
    {
        public string UserName
        {
            get
            {
                return User == null ? null : User.Identity.GetUserName();
            }
        }

    }
}