﻿using System.Web.Optimization;

namespace Mikud_Manage
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //-------------------------------JAVASCRIPT---------------------------------

            //JS-DATATABLE
            bundles.Add(new ScriptBundle("~/bundles/datatable").IncludeDirectory(
                        "~/assets/js/DataTables", "*.js", true));

            //JS-BEYOND
            bundles.Add(new ScriptBundle("~/bundles/beyond").Include(
                        "~/assets/js/beyond.min.js"));

            //JS-SKINS
            bundles.Add(new ScriptBundle("~/bundles/skin").Include(
                        "~/assets/js/skins.min.js"));

            //JS-JQUERY
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/assets/js/jquery.min.js"));

            //JS-JQUERY-UI
            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
                        "~/assets/js/jquery-ui-1.11.4.js"));

            //JS-JQUERYVAL
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/assets/js/jqueryval/jquery.validate*"));

            //JS-MODERNIZER
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/assets/js/modernizr-*"));

            //JS-BOOTSTRAP
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/assets/js/bootstrap.min.js",
                        "~/assets/js/slimscroll/jquery.slimscroll.min.js",
                        "~/assets/js/bootstrap-modal.js",
                        "~/assets/js/bootstrap-modalmanager.js"
                        ));

            //JS-DATETIMEPICKER
            bundles.Add(new ScriptBundle("~/bundles/datetimepicker").Include(
                        "~/assets/js/bootstrap-datetimepicker.js",
                        "~/assets/js/bootstrap-datepicker.js",
                        "~/assets/js/datetimepicker-setup.js",
                        "~/assets/js/he.js",
                        "~/assets/js/respond.js",
                        "~/assets/js/datetimepicker-aditional-scripts.js"));

            //JS-MOMENT
            bundles.Add(new ScriptBundle("~/bundles/moment").Include(
                        "~/assets/js/moment.js"));

            //JS-FULLCALENDAR
            bundles.Add(new ScriptBundle("~/bundles/fullcalendar").Include(
                        "~/assets/js/fullcalendar/fullcalendar.js",
                        "~/assets/js/fullcalendar/fullcalendar-additional-scripts.js"));

            //JS-JTABLE
            bundles.Add(new ScriptBundle("~/bundles/jtable").Include(
                "~/assets/js/jtable/jquery-1.6.4.min.js",
                "~/assets/js/jtable/jquery-ui-1.8.16.custom.min.js",
                        "~/assets/js/modernizr-2.6.2.js",
                        "~/assets/js/jtable/syntaxhighligher/shCore.js",
                        "~/assets/js/jtable/syntaxhighligher/shBrushJScript.js",
                        "~/assets/js/jtable/syntaxhighligher/shBrushXml.js",
                        "~/assets/js/jtable/syntaxhighligher/shBrushCSharp.js",
                        "~/assets/js/jtable/syntaxhighligher/shBrushSql.js",
                        "~/assets/js/jtable/jtablesite.js",
                        "~/assets/js/jtable/jtable/jquery.jtable.js",
                        "~/assets/js/jtable/validationEngine/jquery.validationEngine.js",
                        "~/assets/js/jtable/validationEngine/jquery.validationEngine-en.js"));

            //--------------------------------CSS---------------------------------

            //CSS-DATATABLE
            bundles.Add(new StyleBundle("~/bundles/datatable").IncludeDirectory(
                "~/assets/css/DataTables/css", "*.css", true));

            //CSS-BOOTSTRAP
            bundles.Add(new StyleBundle("~/css/bootstrap").Include(
                        "~/assets/css/bootstrap.min.css",
                        "~/assets/css/bootstrap-modal.css",
                        "~/assets/css/bootstrap-modal-bs3patch.css"
                        ));

            //CSS-BOOTSTRAP-RTL
            bundles.Add(new StyleBundle("~/css/bootstrap-rtl").Include(
                        "~/assets/css/bootstrap-rtl.min.css"));

            //CSS-DATETIMEPICKER
            bundles.Add(new StyleBundle("~/css/datetimepicker").Include(
                        "~/assets/css/bootstrap-datetimepicker.css"));

            //CSS-BEYOND
            bundles.Add(new StyleBundle("~/css/beyond").Include(
                        "~/assets/css/beyond.min.css",
                        "~/assets/css/demo.min.css",
                        "~/assets/css/font-awesome.min.css",
                        "~/assets/css/typicons.min.css",
                        "~/assets/css/weather-icons.min.css",
                        "~/assets/css/animate.min.css"));

            //CSS-BEYOND-RTL
            bundles.Add(new StyleBundle("~/css/beyond-rtl").Include(
                        "~/assets/css/beyond-rtl.min.css",
                        "~/assets/css/demo.min.css",
                        "~/assets/css/font-awesome.min.css",
                        "~/assets/css/typicons.min.css",
                        "~/assets/css/weather-icons.min.css",
                        "~/assets/css/animate.min.css"));

            //CSS-SITE
            bundles.Add(new StyleBundle("~/css/site").Include(
                        "~/assets/css/site.css"));

            //CSS-FULLCALENDAR
            bundles.Add(new StyleBundle("~/css/fullcalendar").Include(
                        "~/assets/css/fullcalendar.css",
                        "~/assets/css/jquery-ui.custom.css"));

            //CSS-JTABLE
            bundles.Add(new StyleBundle("~/css/jtable").Include(
                        "~/assets/css/site.css",
                        "~/assets/css/jquery-ui-1.8.16.custom.css",
                        "~/assets/css/highlight.css",
                        "~/assets/js/jtable/syntaxhighligher/styles/shCore.css",
                        "~/assets/js/jtable/syntaxhighligher/styles/shThemeDefault.css",
                        "~/assets/js/jtable/validationEngine/validationEngine.jquery.css"));
        }
    }
}
