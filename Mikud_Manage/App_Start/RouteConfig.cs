﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Mikud_Manage
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("elmah.axd");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Guards", action = "ProfilePage", id = UrlParameter.Optional }
            );
        }
    }
}
