﻿
using System;
using System.ComponentModel;
namespace Mikud_Manage.Models
{
    public class Address
    {
        public int ID { get; set; }
        [DisplayName("מספר דירה")]
        public int FlatNumber { get; set; }
        [DisplayName("מספר בית")]
        public int HouseNumber { get; set; }
        [DisplayName("רחוב")]
        public String Street { get; set; }
        public int CityID { get; set; }
        public virtual City City { get; set; }
        [DisplayName("עדקנית")]
        public bool Actual { get; set; }

        public int GuardID { get; set; }
        public virtual Guard Guard { get; set; }
    }
}