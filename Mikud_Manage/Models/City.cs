﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Mikud_Manage.Models
{
    public class City
    {
        public int ID { get; set; }
        [DisplayName("עיר")]
        public String Name { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
    }
}