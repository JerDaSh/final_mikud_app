﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mikud_Manage.Models
{
    public class Schedual
    {

        public int ID { get; set; }

        [DisplayName("שעת התחלה")]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Start { get; set; }

        [DisplayName("שעת סיום")]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime End { get; set; }


        public virtual int PositionID { get; set; }
        public virtual Position Position { get; set; }


        public virtual int GuardID { get; set; }
        public virtual Guard Guard { get; set; }
    }
}