﻿using System.Data.Entity;

namespace Mikud_Manage.Models
{
    public class CalendarContext : DbContext
    {
        public CalendarContext()
            : base("CalendarContext")
        {
            System.Data.Entity.Database.SetInitializer(new CreateDatabaseIfNotExists<CalendarContext>());
        }
        public System.Data.Entity.DbSet<Mikud_Manage.ViewModels.Appointment> Appointments { get; set; }
    }
}