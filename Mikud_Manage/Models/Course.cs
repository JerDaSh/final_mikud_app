﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mikud_Manage.Models
{
    public class Course
    {
        public int ID { get; set; }
        [DisplayName("תאריך של רענון האחרון")]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime LastTrainingDate { get; set; }
        [DisplayName("תדירות של רענונים")]
        public int TrainingFriquanceInMonth { get; set; }
        public virtual int GuardID { get; set; }
        public virtual Guard Guard { get; set; }
    }
}