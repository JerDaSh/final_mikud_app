﻿
namespace Mikud_Manage.Models
{
    public class OptionsCalendarViewModel
    {
        public int guardId { get; set; }
        public string date { get; set; }
        public string title { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public bool allDay { get; set; }
        public string eventId { get; set; }
    }
}