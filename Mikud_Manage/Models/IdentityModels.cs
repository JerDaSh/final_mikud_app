﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Mikud_Manage.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Address> Addresses { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.City> Cities { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Guard> Guards { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Course> Courses { get; set; }


        public System.Data.Entity.DbSet<Mikud_Manage.Models.Job> Jobs { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Option> Options { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Position> Positions { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Post> Posts { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Schedual> Scheduals { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Standard> Standards { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.TransportationInfo> TransportationInfoes { get; set; }
        public System.Data.Entity.DbSet<Mikud_Manage.Models.File> Files { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Log> Logs { get; set; }

        public System.Data.Entity.DbSet<Mikud_Manage.Models.Message> Messages { get; set; }

    }
}