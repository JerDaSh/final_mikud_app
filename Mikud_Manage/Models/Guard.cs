﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikud_Manage.Models
{
    public class Guard
    {
        [Key]
        public int ID { get; set; }

        /// <summary>
        /// Foreign Key
        /// </summary>
        [StringLength(128), MinLength(3)]
        [ForeignKey("AspNetUser")]
        public virtual string AspNetUserId { get; set; }


        public ApplicationUser AspNetUser { get; set; }
        [DisplayName("שם")]
        public String FirstName { get; set; }
        [DisplayName("שם משפחה")]
        public String LastName { get; set; }
        [DisplayName("תאריך לידה")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Birthday { get; set; }

        [DisplayName("תאריך תחילת עבודה")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime StartWorkingDate { get; set; }
        [DisplayName("מספר פלאפון")]
        [DataType(DataType.PhoneNumber)]
        public String PhoneNumber { get; set; }

        [DisplayName("נקודות")]
        public int Points { get; set; }

        public int JobID { get; set; }
        public virtual Job Job { get; set; }
        public virtual ICollection<Option> Options { get; set; }
        public virtual ICollection<File> Files { get; set; }
        public virtual ICollection<Log> Logs { get; set; }

        public virtual ICollection<Schedual> Scheduals { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
        public virtual ICollection<TransportationInfo> TransportationInfos { get; set; }
        public virtual ICollection<TransportationInfo> Addresses { get; set; }

    }
}
