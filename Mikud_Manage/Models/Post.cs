﻿using System;
using System.ComponentModel;

namespace Mikud_Manage.Models
{
    public class Post
    {
        public int ID { get; set; }
        public int GuardID { get; set; }
        public Guard Guard { get; set; }
        [DisplayName("הודעה")]
        public String PostNote { get; set; }

        [DisplayName("נוצרה")]
        public DateTime Time { get; set; }
    }
}