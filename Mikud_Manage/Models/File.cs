﻿using System.ComponentModel.DataAnnotations;

namespace Mikud_Manage.Models
{
    public class File
    {
        public int FileId { get; set; }
        [StringLength(255)]
        public string FileName { get; set; }
        [StringLength(100)]
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public FileType FileType { get; set; }
        public virtual int GuardID { get; set; }
        public virtual Guard Guard { get; set; }
    }
}