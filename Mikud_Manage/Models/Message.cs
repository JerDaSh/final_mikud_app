﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mikud_Manage.Models
{
    public class Message
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "ל-")]
        public string Reciever { get; set; }

        [Display(Name = "מ-")]
        public string Sender { get; set; }

        [Required]
        [Display(Name = "נושא")]
        public string Subject { get; set; }

        [Required]
        [Display(Name = "גוף ההודעה")]
        public string MessageBody { get; set; }

        public string IsRead { get; set; }

        [Display(Name = "תאריך שליחה")]
        public DateTime DateSend { get; set; }

    }
}