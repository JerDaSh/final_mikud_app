﻿using System.Collections.Generic;

namespace Mikud_Manage.Models.Repositories
{
    public interface IRepositoryScedual
    {
        void Add(Schedual schedual);

        ICollection<Schedual> GetAll();
    }
}
