﻿using System.Collections.Generic;

namespace Mikud_Manage.Models.Repositories
{
    public interface IRepositoryMessages
    {
        void Add(Message message);

        ICollection<Message> GetAll();
    }
}
