﻿using System.Data.Entity;

namespace Mikud_Manage.Models.Repositories
{
    public class RepositoryMessagesMock : RepositoryMock<Message>
    {
        public RepositoryMessagesMock(DbContext db) : base(db)
        {
        }
    }
}

