﻿using System.Collections.Generic;

namespace Mikud_Manage.Models.Repositories
{
    public interface IRepositoryGuards
    {
        void Add(Guard guard);

        ICollection<Guard> GetAll();
    }
}
