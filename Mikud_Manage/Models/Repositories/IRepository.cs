﻿using System;
using System.Linq;

namespace Mikud_Manage.Models.Repositories
{
    public interface IRepository<T> where T : class, new()
    {
        IQueryable<T> GetAll();
        void Delete(int id);
        void Add(T t);
        T Get(int id);
        IQueryable<T> Get(Func<T, bool> predicate);
    }
}
