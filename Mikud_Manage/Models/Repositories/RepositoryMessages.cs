﻿using System.Data.Entity;

namespace Mikud_Manage.Models.Repositories
{
    public class RepositoryMessages : Repository<Message>
    {
        public RepositoryMessages(DbContext db)
            : base(db)
        {
        }
    }
}
