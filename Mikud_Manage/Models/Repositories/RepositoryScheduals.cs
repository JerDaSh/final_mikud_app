﻿using System.Data.Entity;

namespace Mikud_Manage.Models.Repositories
{
    public class RepositoryScheduals : Repository<Schedual>
    {
        public RepositoryScheduals(DbContext db)
            : base(db)
        {
        }
    }
}
