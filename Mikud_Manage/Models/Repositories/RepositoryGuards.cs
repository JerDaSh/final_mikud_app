﻿using System.Data.Entity;

namespace Mikud_Manage.Models.Repositories
{
    public class RepositoryGuards : Repository<Guard>
    {
        public RepositoryGuards(DbContext db)
            : base(db)
        {
        }
    }
}
