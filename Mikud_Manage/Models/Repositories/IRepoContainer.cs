﻿
namespace Mikud_Manage.Models.Repositories
{
    public interface IRepoContainer
    {
        IRepository<Message> RepositoryMessage { get; set; }
    }
}
