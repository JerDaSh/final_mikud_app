﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mikud_Manage.Models.Repositories
{
    public class RepositoryMock<T> : IRepository<T> where T : class, new()
    {
        private readonly List<T> _list;

        public RepositoryMock(DbContext db)
        {
            _list = new List<T>();
        }

        public IQueryable<T> GetAll()
        {
            return _list.AsQueryable();
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public void Add(T t)
        {
            _list.Add(t);
        }

        public T Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<T> Get(Func<T, bool> predicate)
        {
            throw new System.NotImplementedException();
        }
    }
}