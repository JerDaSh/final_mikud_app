﻿using System.Data.Entity;

namespace Mikud_Manage.Models.Repositories
{
    public class RepoContainer : IRepoContainer
    {
        private readonly DbContext _context;
        public IRepository<Message> RepositoryMessage { get; set; }

        public RepoContainer(DbContext context)
        {
            _context = context;
            RepositoryMessage = new RepositoryMessages(context);
        }
    }
}