﻿using System;
using System.Data.Entity;
using System.Linq;


namespace Mikud_Manage.Models.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : class, new()
    {
        protected DbSet<T> Entity;

        public DbContext Db { get; set; }

        protected Repository(DbContext db)
        {
            Db = db;
            Entity = Db.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return Entity.AsQueryable();
        }

        public void Delete(int id)
        {
            T t = Entity.Find(id);
            Entity.Remove(t);
            Db.SaveChanges();
        }

        public void Add(T t)
        {
            Entity.Add(t);
            Db.SaveChanges();
        }

        public T Get(int id)
        {
            return Entity.Find(id);
        }

        public IQueryable<T> Get(Func<T, bool> predicate)
        {
            return Entity.Where(predicate).AsQueryable();
        }
    }
}