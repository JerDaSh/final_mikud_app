﻿using System;
using System.ComponentModel;

namespace Mikud_Manage.Models
{
    public class Log
    {
        public int ID { get; set; }
        [DisplayName("הודעה")]
        public String Descreption { get; set; }
        [DisplayName("זמן הפעולה")]
        public DateTime Time { get; set; }
    }
}