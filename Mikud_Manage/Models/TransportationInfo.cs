﻿using System;
using System.ComponentModel;

namespace Mikud_Manage.Models
{
    public class TransportationInfo
    {
        public int ID { get; set; }
        [DisplayName("קיים רכב")]
        public bool HasACar { get; set; }

        [DisplayName("צריך הסעה")]
        public bool NeedATransportation { get; set; }

        [DisplayName("ניקודת איסוף")]
        public String CollectionPoint { get; set; }

        public int CityID { get; set; }
        public virtual City City { get; set; }

        [DisplayName("עדקני")]
        public bool Actual { get; set; }

        public virtual int GuardID { get; set; }
        public virtual Guard Guard { get; set; }
    }
}