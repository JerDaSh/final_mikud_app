﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mikud_Manage.Models
{
    public class Standard
    {
        [DisplayName("תאריך ושעת התחלה")]
        public DateTime StartTime { get; set; }

        [DisplayName("תאריך ושעת סיום")]
        public DateTime EndTime { get; set; }

        public virtual int PositionID { get; set; }
        public virtual Position Position { get; set; }

        public int ID { get; set; }
        public int JobID { get; set; }
        public virtual Job Job { get; set; }
    }
}