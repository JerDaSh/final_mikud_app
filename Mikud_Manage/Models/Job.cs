﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Mikud_Manage.Models
{
    public class Job
    {
        public int ID { get; set; }
        [DisplayName("תפקיד")]
        public String Name { get; set; }

        [DisplayName("צריך רענון")]
        public bool NeedAtrainning { get; set; }
        public virtual ICollection<Guard> Guards { get; set; }
        public virtual ICollection<Standard> Standards { get; set; }
    }
}