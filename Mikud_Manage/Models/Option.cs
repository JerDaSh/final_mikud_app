﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mikud_Manage.Models
{
    public class Option
    {
        public int ID { get; set; }
        [DisplayName("תאריך ושעת התחלה")]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime Start { get; set; }

        [DisplayName("תאריך ושעת סיום")]
        [DisplayFormat(DataFormatString = "{0:dd/mm/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime End { get; set; }
        public string EvenetId { get; set; }
        public string Title { get; set; }
        public virtual int GuardID { get; set; }
        public virtual Guard Guard { get; set; }
    }
}