﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Mikud_Manage.Models
{
    public class Position
    {
        public int ID { get; set; }
        [DisplayName("עמדה")]
        public String Name { get; set; }
        public virtual ICollection<Standard> Standards { get; set; }
        public virtual ICollection<Schedual> Scheduals { get; set; }
    }
}